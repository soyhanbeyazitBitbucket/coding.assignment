﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


$(document).ready(function () {
    $("#search").click(startSearching);
    $("#loadMore").click(loadVideos);
    $('#trailerModal').on('hidden.bs.modal', removeVideoFromFrame);
    $("#query").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#search").trigger("click");
        }
    });
});

var startSearching = function () {
    $("#currentPage").val("0");
    loadVideos();
}

var loadVideos = function () {
    
    if (!validate())
        return;

    $("#search i").addClass("fa-spin");
    $("#search").attr("disabled");
    var pageNumber = 1;

    if ($("#lastQueryText").val() == $("#query").val() || $("#lastQueryText").val() == "")
        pageNumber = Number.parseInt($("#currentPage").val()) + 1;

    var requestData = {
        Query: $("#query").val(),
        Page: pageNumber
    };

    $.ajax({
        url: "/Home/search",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(requestData),
        success: updateResults,
        error: function () {
            noResult("Search yielded no result");
        }
    });
};

var updateResults = function (data) {
    $("#queryTextHeader").text(data.query);

    if (data.page == 1) {
        $("#results").empty();
    }

    data.items.forEach(function (element) {
        var videoItem = `<div class='col-md-4 card-container'>`;
        videoItem += "      <div class='card' style='width: 18rem;'>";
        videoItem += `            <img class='card-img-top' src = '${element.imageUrl}' alt = 'Card image cap'>`;
        videoItem += `            <div class='card-body'>`;
        videoItem += `                <h5 class='card-title'>${element.title}</h5>`;
        videoItem += `                <p class='card-text'>${element.year}</p>`;
        videoItem += `                <p class='card-text'>${element.source}</p>`;
        videoItem += `               <a href='#' class='btn btn-outline-primary btn-sm btn-show-trailer' data-film-key='${element.trailerUrl}'><i class="fas fa-film"></i> Watch Trailer</a>`;
        videoItem += `            </div>`;
        videoItem += `      </div>`;
        videoItem += `  </div>`;


        $("#results").append(videoItem);

        $(".btn-show-trailer:last").click(function () {
            console.log(element);
            var videoShareUrl = element.trailer.shareUrl;
            var modalTitle = `${element.title} - ${element.year}`;
            $("#trailerModal .modal-body .row").html(element.trailer.iFrame);
            $(".modal-title").html(modalTitle);
            twttr.widgets.createShareButton(videoShareUrl, document.getElementById('btnTwitterShare'),
                {
                    text: element.title
                }
            );

            $("#trailerModal .modal-body iframe").removeClass("invisible");
            $("#trailerModal").modal("show");
        });
    });

    $("#currentPage").val(data.page);
    $("#lastQueryText").val(data.query);
    $("#loadMore").removeClass("invisible");

    waitingForSearch();
};

var validate = function () {
    var validateItem = $("#query");
    if (validateItem.val().length == 0) {
        validateItem.addClass("is-invalid");
        return false;
    }
    else {
        validateItem.removeClass("is-invalid");

        return true;
    }
}

var removeVideoFromFrame = function () {
    $("#btnTwitterShare").html("");
    $("#trailerModal .modal-body row").html("");

};

var noResult = function (message) {
    waitingForSearch();
    alert(message);

}

var waitingForSearch = function () {
    $("#search i").removeAttr("disabled");
    $("#search i").removeClass("fa-spin");
}

//Twitter script include
window.twttr = (function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function (f) {
        t._e.push(f);
    };

    return t;
}(document, "script", "twitter-wjs"));
//Twitter script include
