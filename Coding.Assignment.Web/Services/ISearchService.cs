﻿using Coding.Assignment.Web.Models;
using System.Collections.Generic;

namespace Coding.Assignment.Web.Services
{
    public interface ISearchService
    {
        List<VideoResultModel> SearchVideos(VideoPageRequestModel data);
    }
}
