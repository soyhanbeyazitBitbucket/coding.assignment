﻿using Coding.Assignment.Web.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;

namespace Coding.Assignment.Web.Services
{
    public class SearchService : ISearchService
    {
        private readonly IConfiguration _configuration;
        public SearchService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public List<VideoResultModel> SearchVideos(VideoPageRequestModel data)
        {
            string apiUrl = _configuration["VideoSearchMiddleware:ApiEndPoint"];

#if DEBUG
            apiUrl = "https://localhost:44319";

#endif

            IRestClient restClient = new RestClient(apiUrl);

            IRestRequest restRequest = new RestRequest("api/Video/Search", Method.POST, DataFormat.Json);
            restRequest.AddQueryParameter("Query", data.Query);
            restRequest.AddQueryParameter("Page", data.Page.ToString());

            var result = restClient.Execute(restRequest);

            List<VideoResultModel> videoList = JsonConvert.DeserializeObject<SearchApiResult>(result.Content).VideoResults;

            return videoList;
        }
    }
}
