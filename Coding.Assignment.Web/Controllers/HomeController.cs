﻿using Coding.Assignment.Web.Models;
using Coding.Assignment.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace Coding.Assignment.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISearchService _searchService;
        public HomeController(ISearchService searchService)
        {
            _searchService = searchService;
        }
        public IActionResult Index()
        {
            ViewBag.Query = "";

            return View();
        }

        [HttpPost]
        public IActionResult Search([FromBody] VideoPageRequestModel data)
        {
            //TODO : try catch

            PagedResponseModel<VideoResultModel> pagedResponseModel = new PagedResponseModel<VideoResultModel>();
            pagedResponseModel.Page = data.Page;
            pagedResponseModel.Query = data.Query;
            pagedResponseModel.Items = _searchService.SearchVideos(data);

            return Json(pagedResponseModel);
        }
    }
}
