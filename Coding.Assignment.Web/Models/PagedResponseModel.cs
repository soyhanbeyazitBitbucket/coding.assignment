﻿using System.Collections.Generic;

namespace Coding.Assignment.Web.Models
{
    public class PagedResponseModel<T> where T : class
    {
        public string Query { get; set; }
        public int Page { get; set; }
        public List<T> Items { get; set; }
    }
}
