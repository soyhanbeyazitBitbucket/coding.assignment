﻿namespace Coding.Assignment.Web.Models
{
    public class VideoPageRequestModel
    {
        public string Query { get; set; }
        public int Page { get; set; }
    }
}
