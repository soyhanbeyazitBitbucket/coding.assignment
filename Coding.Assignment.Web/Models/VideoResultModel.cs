﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Coding.Assignment.Web.Models
{
    public class VideoResultModel
    {
        private string _imageUrl;

        public string Title { get; set; }
        public string Year { get; set; }
        public string Source { get; set; }
        public string Key { get; set; }
        public string ImageUrl
        {
            get { return _imageUrl; }
            set { _imageUrl = value.Length > 4 ? value : "https://www.flixdetective.com/web/images/poster-placeholder.png"; }
        }
        public Trailer Trailer { get; set; }

    }

    public class SearchApiResult
    {
        [JsonProperty("data")]
        public List<VideoResultModel> VideoResults { get; set; }
    }

    public class Trailer
    {
        public string Key { get; set; }
        public string IFrame { get; set; }
        public string ShareUrl { get; set; }
    }
}
