# Coding.Assignment

Web App : http://codingassignmentwebapp.azurewebsites.net/#  
Middleware Endpoint: http://codingassignmentapi.azurewebsites.net/swagger/index.html  

### Tech Stack :  

* Visual Studio 2017  
* Bitbucket  
* C# .NET Core 2.1.4  
* Html, CSS, Javascript, Jquery  
* Bootstrap 4  
* Azure Cloud for App delivery; App services (Free tier) . 

### APIs for Trailer data retrieval :  

* OMDB api : http://www.omdbapi.com
* Youtube Api : https://developers.google.com/youtube/  
* Vimeo Api : https://developer.vimeo.com  

# Requirements

## Case Backend Developers
### How to submit:

1. Upload your solution to (private) Github Repository or BitBucket
2. Invite software@ogd.nl to it so we can assess you work

### Assignment
Create a webpage on which you can search for movie trailers.

* Use an API of an online movie database (e.g. IMDB or Rotten Tomatoes);
* Use an API of an online video service (e.g. YouTube or Vimeo);
* Create your own WebAPI as middleware to retrieve the results of both services and aggregate them;
* Cache the aggregated data for performance;
* Make the search as smart as you can;

#### Tips
* Showing a lot of embedded videos on your page increases page load time significantly;
* BONUS: Add the option to share trailers on social networks.
