﻿using Coding.Assignment.AppServices;
using Coding.Assignment.AppServices.Search;
using Coding.Assignment.AppServices.Trailer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Coding.Assignment.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Swagger
            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info()
                {
                    Description = "Video Search. Results are gathered and aggregated in one api",
                    Title = "Video Search API",
                    Version = "v1"
                });
            });

            services.AddScoped<IVideoSearchAppService, OmdbSearchAppService>();

            //Inject Active Trailer Search Service
            SearchAppServiceFactory searchAppServiceFactory = new SearchAppServiceFactory();

            services.AddScoped(typeof(ITrailerSearchAppService), searchAppServiceFactory.GetSearchAppService(Configuration["ActiveSearchService"]));

            //Vimeo Trailer Search
            //services.AddScoped<ITrailerSearchAppService, VimeoSearchAppService>();


            services.AddMemoryCache();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            //Swagger use
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");
            });
        }
    }
}
