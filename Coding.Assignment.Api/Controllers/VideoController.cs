﻿using Coding.Assignment.Api.Models;
using Coding.Assignment.AppServices.Model;
using Coding.Assignment.AppServices.Search;
using Coding.Assignment.AppServices.Trailer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coding.Assignment.Api.Filters;

namespace Coding.Assignment.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [GeneralApiException]
    public class VideoController : ControllerBase
    {
        private readonly IVideoSearchAppService _videoSearchAppService;
        private readonly ITrailerSearchAppService _trailerSearchAppService;
        private readonly IConfiguration _configuration;

        private IMemoryCache _memoryCache;

        public VideoController(IVideoSearchAppService videoSearchAppService, ITrailerSearchAppService trailerSearchAppService, IMemoryCache memoryCache, IConfiguration configuration)
        {
            _videoSearchAppService = videoSearchAppService;
            _trailerSearchAppService = trailerSearchAppService;
            _configuration = configuration;
            _memoryCache = memoryCache;

        }

        [HttpPost]
        [SwaggerOperation("This web api method will return aggregated data collected from Video platforms api's")]
        [SwaggerResponse(200, Type = typeof(Result<List<VideoModel>>))]
        [SwaggerResponse(500, Type = typeof(Result<string>))]

        public Task<Result<List<VideoModel>>> Search([FromQuery] PagedVideoSearchModel videoSearchModel)
        {
            Result<List<VideoModel>> cached = new Result<List<VideoModel>>();
            try
            {
                string currentRequestCacheKey = videoSearchModel.GetCacheKey(_configuration);

                string additionalTrailerSearchKeywords = _configuration["AdditionalTrailerSearchKeywords"];


                if (!_memoryCache.TryGetValue(currentRequestCacheKey, out cached))
                {

                    Result<List<VideoModel>> result = new Result<List<VideoModel>>();
                    result.Status = 200;
                    result.Data = _videoSearchAppService.Search(videoSearchModel);

                    Parallel.For(0, result.Data.Count, i =>
                     {
                         //Searching Trailer for a Movie
                         string searchKeyword = result.Data[i].Title + " " + additionalTrailerSearchKeywords;
                         result.Data[i].Trailer = _trailerSearchAppService.Search(new PagedVideoSearchModel() { Query = searchKeyword });

                     });

                    //Filter out no trailer data
                    result.Data = result.Data.Where(a => a.Trailer != null).ToList();

                    cached = result;

                    _memoryCache.Set(currentRequestCacheKey, cached);


                }

                return Task.FromResult(cached);

            }
            catch (Exception ex)
            {
                cached.Message = "Internal server occurred ex : " + ex.Message;
                cached.Status = 500;
                cached.Data = null;
                this.Response.StatusCode = 500;
                return Task.FromResult(cached);
            }
        }

    }
}