﻿using System.Threading.Tasks;
using Coding.Assignment.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Coding.Assignment.Api.Filters
{
    public class GeneralApiException : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            Result<string> result = new Result<string>();
            result.Message = context.Exception.Message;
            result.Status = 500;
            result.Data = context.Exception.StackTrace;

            context.Result = new JsonResult(result);
            context.HttpContext.Response.StatusCode = result.Status;

            base.OnException(context);
        }

        public override Task OnExceptionAsync(ExceptionContext context)
        {
            return base.OnExceptionAsync(context);
        }
    }
}
