﻿namespace Coding.Assignment.Api.Models
{
    public class VideoResultModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumbnailUrl { get; set; }
        public string VideoFullUrl { get; set; }
        public string Source { get; set; }
    }
}
