﻿using System.ComponentModel.DataAnnotations;

namespace Coding.Assignment.Api.Models
{
    public class VideoSearchModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Query string should be specified.")]
        public string Query { get; set; }
        [Range(1,999,ErrorMessage ="Page number should between 1-999.")]
        public int Page { get; set; }

    }
}
