using Coding.Assignment.AppServices.Model;
using Coding.Assignment.AppServices.Trailer;
using Xunit;

namespace Coding.Assignment.Test
{
    public class YoutubeApiSearchAppServiceTest
    {
        public YoutubeApiSearchAppServiceTest()
        {
        }
        [Fact]
        public void YoutubeSearchTest()
        {
            YoutubeSearchAppService youtubeSearchAppService = new YoutubeSearchAppService(TestHelper.InitConfiguration());

            PagedVideoSearchModel pagedVideoSearchModel = new PagedVideoSearchModel();

            pagedVideoSearchModel.Page = 1;
            pagedVideoSearchModel.Query = "Titanic";

            TrailerModel results = youtubeSearchAppService.Search(pagedVideoSearchModel);

            Assert.True(results.Key.Length > 0);
        }
    }
}
