using Coding.Assignment.AppServices.Model;
using Coding.Assignment.AppServices.Search;
using System.Collections.Generic;
using Xunit;

namespace Coding.Assignment.Test
{
    public class OmdbApiSearchAppServiceTest
    {
        public OmdbApiSearchAppServiceTest()
        {
        }
        [Fact]
        public void OmdbSearchTest()
        {
            OmdbSearchAppService omdbSearchAppService = new OmdbSearchAppService(TestHelper.InitConfiguration());

            PagedVideoSearchModel pagedVideoSearchModel = new PagedVideoSearchModel();
            
            pagedVideoSearchModel.Page = 1;
            pagedVideoSearchModel.Query = "Titanic";

            List<VideoModel> results = omdbSearchAppService.Search(pagedVideoSearchModel);

            Assert.True(results.Count > 0);
        }
    }
}
