using Coding.Assignment.AppServices;
using Coding.Assignment.AppServices.Trailer;
using System;
using Xunit;

namespace Coding.Assignment.Test
{
    public class SearchServiceFactoryTest
    {
        public SearchServiceFactoryTest()
        {
        }
        [Fact]
        public void VimeoSearchTest()
        {
            SearchAppServiceFactory searchAppServiceFactory = new SearchAppServiceFactory();

            var actual = searchAppServiceFactory.GetSearchAppService("Vimeo");
            var expected = typeof(VimeoSearchAppService);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void YoutubeSearchTest()
        {
            SearchAppServiceFactory searchAppServiceFactory = new SearchAppServiceFactory();

            var actual = searchAppServiceFactory.GetSearchAppService("Youtube");
            var expected = typeof(YoutubeSearchAppService);
            Assert.Equal(expected,actual);
        }

        [Fact]
        public void DefaultSearchTest()
        {
            SearchAppServiceFactory searchAppServiceFactory = new SearchAppServiceFactory();

            var actual = searchAppServiceFactory.GetSearchAppService("anything");
            var expected = typeof(YoutubeSearchAppService);
            Assert.Equal(expected, actual);
        }
    }
}
