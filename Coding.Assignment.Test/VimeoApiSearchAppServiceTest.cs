using Coding.Assignment.AppServices.Model;
using Coding.Assignment.AppServices.Trailer;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using Xunit;

namespace Coding.Assignment.Test
{
    public class VimeoApiSearchAppServiceTest
    {
        public VimeoApiSearchAppServiceTest()
        {
        }
        [Fact]
        public void VimeoSearchTest()
        {
            var memoryCacheMock = new Mock<IMemoryCache>();

            VimeoSearchAppService vimeoSearchAppService = new VimeoSearchAppService(TestHelper.InitConfiguration(), memoryCacheMock.Object);

            TrailerModel videoModel = vimeoSearchAppService.Search(new PagedVideoSearchModel()
            {
                Page = 1,
                Query = "James Bond Trailer"
            });

            Assert.True(videoModel.Key.Length > 0);
        }

        [Fact]
        public void VimeoSearchToken()
        {
            var memoryCacheMock = new Mock<IMemoryCache>();

            VimeoSearchAppService vimeoSearchAppService = new VimeoSearchAppService(TestHelper.InitConfiguration(), memoryCacheMock.Object);

            string token = vimeoSearchAppService.GetToken().AccessToken;

            Assert.True(token.Length > 0);
        }
    }
}
