﻿using Coding.Assignment.AppServices.Trailer;
using Microsoft.Extensions.Configuration;
using System;

namespace Coding.Assignment.AppServices
{
    public class SearchAppServiceFactory
    {
        public SearchAppServiceFactory()
        {
        }
        public Type GetSearchAppService(string type)
        {
            switch (type)
            {
                case "Youtube":
                    return typeof(YoutubeSearchAppService);
                case "Vimeo":
                    return typeof(VimeoSearchAppService);
                default:
                    return typeof(YoutubeSearchAppService);
            }
        }
    }
}
