﻿using Coding.Assignment.AppServices.Model;

namespace Coding.Assignment.AppServices.Trailer
{
    public interface ITrailerSearchAppService
    {
        TrailerModel Search(PagedVideoSearchModel pagedVideoSearchModel);
    }
}
