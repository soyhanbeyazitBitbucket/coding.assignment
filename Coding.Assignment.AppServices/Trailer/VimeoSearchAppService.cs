﻿using Coding.Assignment.AppServices.Model;
using Coding.Assignment.AppServices.Model.Vimeo;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;

namespace Coding.Assignment.AppServices.Trailer
{

    public class VimeoSearchAppService : ITrailerSearchAppService
    {
        public string AppAuthorizationKey
        {
            get
            {
                return "basic " + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_configuration["Vimeo:ClientId"] + ":" + _configuration["Vimeo:ClientSecret"]));
            }
        }

        public string ApiEndpoint
        {
            get
            {
                return _configuration["Vimeo:ApiEndpoint"];
            }
        }

        public string IFrame
        {
            get
            {
                return _configuration["Vimeo:IFrame"];
            }
        }

        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public VimeoSearchAppService(IConfiguration configuration, IMemoryCache memoryCache)
        {
            _configuration = configuration;
            _memoryCache = memoryCache;

        }

        public TrailerModel Search(PagedVideoSearchModel pagedVideoSearchModel)
        {
            string token = $"Bearer {this.GetToken().AccessToken}";

            IRestClient client = new RestClient(this.ApiEndpoint);

            IRestRequest restRequest = new RestRequest("videos", Method.GET);
            restRequest.AddHeader("Authorization", token);
            restRequest.AddQueryParameter("query", pagedVideoSearchModel.Query);
            restRequest.AddQueryParameter("page", "1");
            restRequest.AddQueryParameter("per_page", "1");
            restRequest.AddQueryParameter("direction", "asc");
            restRequest.AddQueryParameter("sort", "relevant");


            var result = client.Execute(restRequest);

            VimeoSearchResultModel vimeoSearchResultModel = JsonConvert.DeserializeObject<VimeoSearchResultModel>(result.Content);

            if (vimeoSearchResultModel.Data.Count > 0)
            {
                return new TrailerModel()
                {
                    Key = vimeoSearchResultModel.Data[0].Uri,
                    ShareUrl = vimeoSearchResultModel.Data[0].Link,
                    Iframe = string.Format(this.IFrame, vimeoSearchResultModel.Data[0].Uri.Replace("/videos/", ""))
                };
            }
            else
                return null;
        }

        public AuthorizationResponseModel GetToken()
        {
            AuthorizationResponseModel cached = new AuthorizationResponseModel();

            if (!_memoryCache.TryGetValue(this.AppAuthorizationKey, out cached))
            {

                IRestClient client = new RestClient(_configuration["Vimeo:ApiEndpoint"]);

                IRestRequest restRequest = new RestRequest("/oauth/authorize/client", Method.POST);
                restRequest.AddHeader("Authorization", this.AppAuthorizationKey);
                restRequest.AddHeader("Content-Type", "application/json");
                restRequest.AddHeader("Accept", "application/vnd.vimeo.*+json;version=3.4");
                restRequest.AddJsonBody(new { grant_type = "client_credentials", scope = "public" });

                var result = client.Execute(restRequest);

                cached = JsonConvert.DeserializeObject<AuthorizationResponseModel>(result.Content);
            }
            return cached;
        }
    }
}
