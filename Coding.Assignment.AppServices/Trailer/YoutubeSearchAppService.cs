﻿using Coding.Assignment.AppServices.Model;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Microsoft.Extensions.Configuration;

namespace Coding.Assignment.AppServices.Trailer
{
    public class YoutubeSearchAppService : ITrailerSearchAppService
    {
        public readonly IConfiguration _configuration;

        public string ApplicationName { get { return _configuration["Youtube:ApplicationName"]; } }
        public string ApiKey { get { return _configuration["Youtube:ApiKey"]; } }
        public string IFrame { get { return _configuration["Youtube:IFrame"]; } }
        public string ShareUrl { get { return _configuration["Youtube:ShareUrl"]; } }


        public YoutubeSearchAppService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public TrailerModel Search(PagedVideoSearchModel pagedVideoSearchModel)
        {
            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = this.ApiKey,
                ApplicationName = this.ApplicationName
            });

            var searchListRequest = youtubeService.Search.List("snippet");
            searchListRequest.Q = pagedVideoSearchModel.Query;
            searchListRequest.Type = "video";
            searchListRequest.MaxResults = 1;
            searchListRequest.VideoDuration = SearchResource.ListRequest.VideoDurationEnum.Short__;
            var searchListResponse = searchListRequest.Execute();

            if (searchListResponse.Items.Count > 0)
            {
                return new TrailerModel()
                {
                    Key = searchListResponse.Items[0].Id.VideoId,
                    Iframe = string.Format(this.IFrame, searchListResponse.Items[0].Id.VideoId),
                    ShareUrl = string.Format(this.ShareUrl, searchListResponse.Items[0].Id.VideoId),
                };
            }
            else
                return null;
        }
    }
}
