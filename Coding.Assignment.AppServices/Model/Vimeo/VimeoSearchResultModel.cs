﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Coding.Assignment.AppServices.Model.Vimeo
{
    public class VimeoSearchResultModel
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("data")]
        public List<VimeoVideoModel> Data { get; set; }
    }
}
