﻿using Newtonsoft.Json;

namespace Coding.Assignment.AppServices.Model.Vimeo
{
    public class AuthorizationResponseModel
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }
}
