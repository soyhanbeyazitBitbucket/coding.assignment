﻿using Newtonsoft.Json;

namespace Coding.Assignment.AppServices.Model.Vimeo
{
    public class VimeoVideoModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("uri")]
        public string Uri { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }
    }
}
