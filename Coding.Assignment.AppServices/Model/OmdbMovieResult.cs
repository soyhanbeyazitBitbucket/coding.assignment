﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Coding.Assignment.AppServices.Model
{
    public class OmdbMovie : IGeneralVideo
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string ImdbId { get; set; }
        public string Poster { get; set; }
        public VideoModel GetVideoModel()
        {
            return new VideoModel()
            {
                Key = this.ImdbId,
                Source = VideoSources.Omdb.ToString(),
                Title = this.Title,
                ImageUrl = this.Poster,
                Year = this.Year
            };
        }
    }

    public class OmdbSearchResult
    {
        [JsonProperty("Search")]
        public List<OmdbMovie> Search { get; set; }
        [JsonProperty("totalResults")]
        public int TotalResults { get; set; }
    }
}
