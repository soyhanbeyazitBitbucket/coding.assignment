﻿namespace Coding.Assignment.AppServices.Model
{
    public class VideoModel
    {
        public string Key { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Year { get; set; }
        public string Source { get; set; }
        public TrailerModel Trailer { get; set; }
    }

    public class TrailerModel
    {
        public string Key { get; set; }
        public string TrailerUrl { get; set; }
        public string Iframe { get; set; }
        public string ShareUrl { get; set; }
    }
}
