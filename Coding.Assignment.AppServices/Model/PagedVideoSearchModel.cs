﻿using Microsoft.Extensions.Configuration;

namespace Coding.Assignment.AppServices.Model
{
    public class PagedVideoSearchModel
    {
        public string Query { get; set; }
        public int Page { get; set; }
        public string GetCacheKey(IConfiguration configuration)
        {
            return (configuration["CacheKey"] + "-" + Query.Replace(" ","") + "-" + Page.ToString()).ToLower();
        }
    }
}
