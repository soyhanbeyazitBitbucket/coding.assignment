﻿namespace Coding.Assignment.AppServices.Model
{
    public interface IGeneralVideo
    {
        VideoModel GetVideoModel();
    }
}
