﻿using Coding.Assignment.AppServices.Model;
using System.Collections.Generic;

namespace Coding.Assignment.AppServices.Search
{
    public interface IVideoSearchAppService
    {
        List<VideoModel> Search(PagedVideoSearchModel pagedVideoSearchModel);
    }
}
