﻿using Coding.Assignment.AppServices.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Linq;

namespace Coding.Assignment.AppServices.Search
{
    public class OmdbSearchAppService : IVideoSearchAppService
    {
        private readonly IConfiguration _configuration;
        public string ApiEndPoint
        {
            get
            {
                return _configuration["Omdb:ApiEndPoint"];
            }
        }
        public string ApiKey
        {
            get
            {
                return _configuration["Omdb:ApiKey"];
            }
        }

        public OmdbSearchAppService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public List<VideoModel> Search(PagedVideoSearchModel pagedVideoSearchModel)
        {
            IRestClient restClient = new RestClient(this.ApiEndPoint);

            RestRequest request = new RestRequest(Method.GET);
            request.Parameters.Add(new Parameter("s", pagedVideoSearchModel.Query,ParameterType.QueryString));
            request.Parameters.Add(new Parameter("apiKey", this.ApiKey, ParameterType.QueryString));
            request.Parameters.Add(new Parameter("page", pagedVideoSearchModel.Page, ParameterType.QueryString));

            var result = restClient.Execute(request);


            OmdbSearchResult omdbSearchResult = JsonConvert.DeserializeObject<OmdbSearchResult>(result.Content);

            return omdbSearchResult.Search.Select(a => a.GetVideoModel()).ToList();
        }
    }
}
